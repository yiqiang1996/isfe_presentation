// 洋葱模型可以看出，中间件的在 await next() 前后的操作，很像数据结构的一种场景——“栈”，先进后出。同时，又有统一上下文管理操作数据。

// 综上所述，可以总结出一下特性。
// 1. 有统一 context
// 2. 操作先进后出
// 3. 有控制先进后出的机制 next
// 4. 有提前结束机制

// let's do a simple implementation using Promise

let context={
    data:[]
}

async function middleware1(ctx, next){
    console.log('action 001');
    ctx.data.push(1);
    await next();
    console.log('action 006');
    ctx.data.push(6);
}


async function middleware2(ctx, next){
    console.log('action 002');
    ctx.data.push(2);
    await next();
    console.log('action 005');
    ctx.data.push(5);
}


async function middleware3(ctx, next){
    console.log('action 003');
    ctx.data.push(3);
    await next();
    console.log('action 004');
    ctx.data.push(4);
}


// async function returns a Promise object

Promise.resolve(middleware1(context, async()=>{
    return Promise.resolve(middleware2(context, async()=>{
        return Promise.resolve(middleware3(context, async()=>{
            return Promise.resolve();
        }))
    }))
})).then(()=>{
    console.log("end");
    console.log('context = ', context)
})





// Promise.resolve {
//     console.log('action 001');
//     ctx.data.push(1);
//     await Promise.resolve {
//         console.log('action 002');
//         ctx.data.push(2);
//         await Promise.resolve {
//             console.log('action 003');
//             ctx.data.push(3);
//             await Promise.resolve()
//             console.log('action 004');
//             ctx.data.push(4);
//         }
//         console.log('action 005');
//         ctx.data.push(5);
//     }
//     console.log('action 006');
//     ctx.data.push(6);
// }

// generalize the whole process and write a function