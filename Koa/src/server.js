const express = require('express');
const path = require('path');
const loggerMiddleware = require('./middlewars/logger');
const indexRouter=require('./routes/index')
const apiRouter=require('./routes/api')

const app = express()

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))
app.use(loggerMiddleware);

app.set('views', path.join(__dirname, "views"));
app.set('view engine', 'hbs');



// app.get('/', (req, res) => {
//     res.render('index');
// });

// app.get('/contact', (req, res) => {
//     res.render('contact');
// })

app.use('/', indexRouter);
app.use('/api', apiRouter);


app.listen(3030, () => {
    console.log("Server listening at http://localhost:3030");
})