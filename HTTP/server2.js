const http=require('http');
const hostname='localhost';
const port=3001;


const server=http.createServer();

server.on("request",(req, res)=>{
    console.log('req is an instance of http.IncomingMessage: ',req instanceof http.IncomingMessage);
    console.log('res is an instance of http.ServerResponse: ',res instanceof http.ServerResponse);

    console.log(req.method, req.url)
    res.statusCode=200;
    res.setHeader('Content-Type','text/html');
    res.end('Hello World');
})

server.listen(port, ()=>{
    console.log(`Server running at http://${hostname}:${port}`)
})