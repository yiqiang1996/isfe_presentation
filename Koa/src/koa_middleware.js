const koa=require('koa');
const Router=require('koa-router');

const app=new koa()
const router=Router(); //actually it is a middleware

//simulate async action
const sleep=(mseconds)=>new Promise((resolve)=>setTimeout(()=>{
    console.log('sleep timeout...');
    resolve("Hello Express");
}, mseconds));

//simulate async error
const simulate_async_error=(mseconds)=>new Promise((resolve, reject)=>setTimeout(()=>{
    console.log('simulate async error...');
    reject(new Error('async error'));
}, mseconds))

app.use(async(ctx, next)=>{
    console.log("===============First Middleware Called");
    await next();
    console.log("===============First Middleware Ended");
})

app.use(async(ctx, next)=>{
    console.log("Second Middleware Called");
    await next();
    console.log("Second Middleware Ended");
})

router.get('/api/test1', async(ctx, next)=>{
    console.log('I am the router middleware => /api/test1')
    let result=await new Promise(res=>{
        console.log("sleep for 1000 ms")
        setTimeout(res, 1000, "Hello Koa.js");
    })
    ctx.body=result;
})

router.get('/api/testerror',async(ctx, next)=>{
    ctx.throw(401, "I am error");
    // ctx.body=await simulate_async_error(2000)
})



//router.routes() must return a function (ctx, next)=>{}
app.use(router.routes())

app.listen(3003, ()=>{
    console.log('Server listening at http://localhost:3003');
});

