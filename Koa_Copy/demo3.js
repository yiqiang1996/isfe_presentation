const http = require('http');
const Emitter = require('events');
const compose = require('./demo2');


/**
 * 通用上下文
 */
const context_prototype={
    _body:null,

    get body(){
        return this._body;
    },

    set body(val){
        this._body=val;
        this.res.end(this._body);
    }

    
}


class SimpleKoa extends Emitter{
    constructor(){
        super();
        this.middlewares=[];
        this.context=Object.create(context_prototype); //single instance this.context.__proto__=context_prototype;
    }

    /**
     * 服务事件监听
     * @param {*} args
     */
    listen(...args){
        const server=http.createServer(this.callback()) //this.callback() return a function (req, res)=>{}
        return server.listen(...args);
    }


    /**
     * 注册使用中间件
     * @param {Function} fn
     */
    use(middleware){
        this.middlewares.push(middleware);
    }

    /**
     * 中间件总回调方法
     */
    callback(){
        if(this.listeners('error').length ===0){
            this.on('error', this.onerror);
        }

        const handleRequest=(req, res)=>{
            let context=this.createContext(req, res);
            let fn=compose(this.middlewares);
            fn(context).catch(err=>this.onerror(err))
        };

        return handleRequest;
    }

    onerror(err){
        console.log(err)
    }


    createContext(req, res){
        let context_every_request = Object.create(this.context); //context_every.__proto__ = this.context
        context_every_request.req = req; //http.IncomingMessage
        context_every_request.res = res; //http.ServerResponse
        return context_every_request;  //context_every_request.__proto__.__proto__=context_prototype 
    }
}

module.exports=SimpleKoa;


