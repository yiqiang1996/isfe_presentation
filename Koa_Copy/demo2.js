function compose(middleware) {

    if (!Array.isArray(middleware)) {
        throw new TypeError('Middleware stack must be an array!');
    }

    return function (ctx, next) {
        let index = -1;

        return dispatch(0);

        // make use of closure/闭包
        // allow all the middlewares have access to the 'ctx' passed in 
        function dispatch(i) {
            if (i < index) {
                return Promise.reject(new Error('next() called multiple times'));
            }
            index = i;

            let fn = middleware[i];

            if (i === middleware.length) {
                fn = next;
            }

            if (!fn) {
                return Promise.resolve();
            }

            try {
                return Promise.resolve(fn(ctx, dispatch.bind(null, i + 1))); //Promise.resolve(next_middleware(ctx,next))
            } catch (err) {
                return Promise.reject(err);
            }
        }
    };
}



//                                                                                                 middleware3    
//                                                                             dispatch(2)         dispatch(2)
//                                                         middleware2         middleware2         middleware2
//                                     dispatch(1)         dispatch(1)         dispatch(1)         dispatch(1)
//                 middleware1         middleware1         middleware1         middleware1         middleware1
// dispatch(0)     dispatch(0)         dispatch(0)         dispatch(0)         dispatch(0)         dispatch(0)

//                                     Middlewares Enter Stack



// middleware3    
// dispatch(2)         dispatch(2)
// middleware2         middleware2         middleware2
// dispatch(1)         dispatch(1)         dispatch(1)         dispatch(1)
// middleware1         middleware1         middleware1         middleware1         middleware1
// dispatch(0)         dispatch(0)         dispatch(0)         dispatch(0)         dispatch(0)         dispatch(0)

//                                     Middlwares Exit Stack 




//test the compose function
let middlewares = [];
let context = {
    data: []
};

middlewares.push(async (ctx, next) => {
    console.log('action 001');
    ctx.data.push(2);
    await next();
    console.log('action 006');
    ctx.data.push(5);
});

middlewares.push(async (ctx, next) => {
    console.log('action 002');
    ctx.data.push(2);
    await next();
    console.log('action 005');
    ctx.data.push(5);
});

middlewares.push(async (ctx, next) => {
    console.log('action 003');
    ctx.data.push(2);
    await next();
    console.log('action 004');
    ctx.data.push(5);
});

const fn = compose(middlewares);

fn(context).then(() => {
    console.log('end');
    console.log('context = ', context);
});

// Expected Results:

// "action 001"
// "action 002"
// "action 003"
// "action 004"
// "action 005"
// "action 006"
// "end"
// "context = { data: [1, 2, 3, 4, 5, 6]}"