const express =require('express')
const asyncHandler=require('express-async-handler')
const createError=require('http-errors')
const fs=require('fs')
const util=require('util')
const path=require('path')

const app=express();

//simulate async action
const sleep=(mseconds)=>new Promise((resolve)=>setTimeout(()=>{
    console.log('sleep timeout...');
    resolve("Hello Express");
}, mseconds));

//simulate async error
const simulate_async_error=(mseconds)=>new Promise((resolve, reject)=>setTimeout(()=>{
    console.log('simulate async error...');
    reject(new Error('async error'));
}, mseconds))

//async function wrapper
// function runAsyncWrapper(callback){
//     return function(req, res, next){
//         callback(req, res, next).catch(next);
//     }
// }

// const readFilePromise=util.promisify(fs.readFile)

//middleware 1
app.use(async(req, res, next)=>{
    console.log(`=============== start ${req.method} ${req.url}`,{query: req.query, body: req.body});
    console.log("First Middleware Called");
    const startTime=Date.now()
    await next();
    const cost=Date.now()-startTime;
    console.log('First Middleware Ended');
    console.log(`=============== end ${req.method} ${req.url} ${res.statusCode} - Cost: ${cost}ms`);
})

//middleware 2
app.use(async (req, res, next)=>{
    console.log("Second Middleware Called");
    await next();
    console.log("Second Middleware Ended");
})

//Router
app.get('/api/test1', async(req, res, next)=>{
    console.log('Router Middleware Called => /api/test1');
    let result=await sleep(2000);
    res.status(200).send(result);
    console.log('Router middleware Ended => /api/test1');
})


//Test sync error
app.get('/api/error', (req, res, next)=>{
    console.log("Router middleware starts => /api/error");
    throw new Error("I am error");
})

// Test async error 1 - don't catch async error
app.get('/api/async_error1', async (req, res, next)=>{
    console.log('Router middleware starts => /api/async_error1');
    let result=await simulate_async_error(2000)
    res.send(result);
})

//Test async error 2 - catch error using try/catch
app.get('/api/async_error2', async (req, res, next)=>{
    console.log('Router middleware starts => /api/async_error2');
    try{
        let result=await simulate_async_error(2000)
        res.status(200).send(result);
    }catch(error){
        next(error)
    }
})

//Test async error 3 - catch error using self-designed wrapper function
// app.get('/api/async_error3', runAsyncWrapper(async(req, res, next)=>{
//     console.log('Router middleware starts => /api/async_error3');
//     let result=await simulate_async_error(2000)
//     res.send(result);
// }))

//Test async error 4 - catch error using 3rd party library
app.get('/api/async_error4', asyncHandler(async(req, res, next)=>{
    console.log('Router middleware starts => /api/async_error4');
    let result=await simulate_async_error(2000);
    res.send(result);
}))


// Test async error 5 - Error handling best practice
// app.get('/api/readFile', asyncHandler(async(req, res, next)=>{
//     let file=req.query.fileName;
//     if(file){
//         const content=await readFilePromise(path.join(__dirname, file),'utf-8')
//         res.status(200).json({
//             status:200,
//             data:content
//         });
//     }else{
//         throw createError(400,"'fileName' is missing in query");
//     }
// }))


app.use('*',(req, res, next)=>{
    next(createError(404, "Page Not Found"))
})

//place your error handler after all other middlewares
app.use(async (err, req, res, next)=>{
    if(err){
        console.log('Last middleware catch error: ', err.message)
        // res.status(500).send(`Server Error  =>  ${req.url}`)
        res.status(err.status || 500);
        res.json({
            status:err.status || 500,
            message:err.message,
            url:req.url
        })
        return
    }

    console.log('Last Middleware Called');
    await sleep(2000);
    next();
    console.log('Last Middleware Ended');
})

app.listen(3004,()=>{
    console.log("Server listening at http://localhost:3004");
})



