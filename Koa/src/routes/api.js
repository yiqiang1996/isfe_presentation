const express = require('express');
const router = express.Router();

router.get('/test1', (req, res) => {
  res.json({ name: '年薪百万', price:'996' });
});

router.post('/test2', (req, res) => {
  res.status(201).json({ msg: 'new start' });
});

module.exports = router;
