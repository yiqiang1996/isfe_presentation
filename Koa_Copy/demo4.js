const SimpleKoa = require('./demo3');
const app = new SimpleKoa();

const sleep = (mseconds) => new Promise((resolve) => setTimeout(() => {
    console.log('sleep timeout...');
    resolve("Hello Koa_Copy");
}, mseconds));

// logger
app.use(async (ctx, next) => {
    console.log('-----------start-------------')
    await next();
    const rt = ctx.res['X-Response-Time'];
    console.log(`${ctx.req.method} ${ctx.req.url} - time cost: ${rt}`);
    console.log('-----------ended-------------')
});

// x-response-time

app.use(async (ctx, next) => {
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    ctx.res['X-Response-Time'] = `${ms}ms`;
});

// response

app.use(async ctx => {
    let result = await sleep(2000);
    ctx.body = result;
});

app.listen(3000, () => {
    console.log("Server started at http://localhost:3000");
});